package shuo;

public class Rotate {

	public static void main(String[] args) {
		int a[] = new int[] { 8, 6, 3, 4, 5 };
		System.out.println("array before rotate");
		printArray(a);
		rotate(a);
		System.out.println("array after rotate");
		printArray(a);

	}

	public static void rotate(int a[]) {
		int b = a[a.length - 1];
		for (int i = a.length - 1; i >= 1; i--) {
			a[i] = a[i - 1];
		}
		a[0] = b;

	}

	public static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + ",");
		}
		System.out.println("");
	}
}
