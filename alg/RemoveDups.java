package shuo;

import java.util.HashSet;
public class RemoveDups {

	public static void main(String[] args) {
		char[] b = new char[]{'d','a','e','a','u','d','a','t'};
		char[] r = removeDuplicates2(b);
		for(int i=0;i<r.length;i++)
			System.out.print(r[i]);
	}
	
	public static char[] removeDuplicates(char[] array){
		int[] dict = new int[128];
		char[] result = new char[array.length];
		int j=0;
		for(int i=0;i<array.length;i++){
			if(dict[array[i]]==0){
				dict[array[i]] = 1;
				result[j] = array[i];
				j++;
			}
		}
		return result;
	}
	
	public static char[] removeDuplicates2(char[] array){
		HashSet dict = new HashSet();
		char[] result = new char[array.length];
		int j=0;
		for(int i=0;i<array.length;i++){
			if(!dict.contains(array[i])){
				dict.add(array[i]);
				result[j] = array[i];
				j++;
			}
		}
		return result;
	}

}
