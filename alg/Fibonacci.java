package shuo;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(fibonacciRecursive(10));
		System.out.println(fibonacciNonRecursive(10));
	}

	public static int fibonacciRecursive(int n){
		if(n==1||n==2)
			return 1;
		else 
			return fibonacciRecursive(n-1)+fibonacciRecursive(n-2);
	}

	public static int fibonacciNonRecursive(int n) {

		if (n == 1 || n == 2)
			return 1;
		else{
			int c = 2;
			int b = 1;
			int a = 1;
			for (int i = 3; i <= n; i++) {
				c = b + a;
				a = b;
				b = c;
			}
			return c;
		}
		
	}
}
