package shuo;

public class Reverse {
	public static void main(String[] args) 
	{
		double a[]=new double []{1,2,3,4,5};
		double[] getReverse=reverse(a);		
		printArray(getReverse);
	}

	public static double[] reverse(double a[])
	{
		for(int i=0;i<a.length/2;i++){
			double temp=a[i];
			a[i]=a[a.length-1-i];
			a[a.length-i-1]=temp;
		}
	
		return a;
	}
	public static void printArray(double[] getReverse){
		for(int i=0;i<getReverse.length;i++)
		{
		System.out.println(getReverse[i]);
		}
	}
}
