package shuo;

public class ArrayMerge {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = new int[] { 1, 3, 5 };
		System.out.print("Array a is:");
		printArray(a);
		int b[] = new int[] { 2, 4, 5, 8, 9 };
		System.out.print("Array b is:");
		printArray(b);
		int c[] = new int[a.length + b.length];
		mergeArray(a,b,c);
		System.out.print("Array c is:");
		printArray(c);

	}

	public static void mergeArray(int a[], int b[], int c[]) {
		
		int i=0,j=0, k=0;
		for(;i<a.length && j<b.length;k++)
		{
			if(a[i]<=b[j])
			{
				c[k]=a[i];
				i++;
			}
			else
			{
				c[k]=b[j];
				j++;
			}
		}
		
		for(;j<b.length;j++,k++)
			c[k]=b[j];
		
		for(;i<a.length;i++,k++)
			c[k]=a[i];
		

	}

	public static void printArray(int c[]) {
		for (int i = 0; i < c.length; i++) {
			System.out.print(c[i] + ",");
		}
		System.out.println("");
	}

}
