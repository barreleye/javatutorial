package shuo;


public class Manager extends Employee{

	
	public static void main(String[] args) {
		//e is an object
		Employee e = new Employee(1,"Mingshuo");
		//e2 is an object
		Employee e2 = new Employee(1,"Jiajie",90000);
		e.salary = 10000;
		e.company = "ITG";
		e2.company = "Chase";
		//System.out.println(e.toString());
		//System.out.println(e2.toString());
		//m is an object
		Employee m = new Manager();
		
		m.setNameSalary("Paul", 136000);
		//System.out.println(m.toString());
		Employee[] staff = new Employee[3];
		staff[0] = e;
		staff[1] = e2;
		staff[2] = m;
		
		for(int i=0;i<staff.length;i++)
			System.out.println(staff[i].toString());
			
		System.out.println(e.equals(e2));
		
	}
	
	
	//override
	public double getSalary()
	{
		return salary*1.1;
		
	}

}
