package shuo;

public class Accountant extends Associate{
	
	public static void main(String[] args)
	{
		Associate a1 = new Programmer();
		Associate a2 = new Accountant();
		Associate a3 = new Cleaner();
		
		a1.daily();
		a2.daily();
		a3.daily();
	}

	public void dowork()
	{
		System.out.println("Do accouting");
	}
	
	
}
