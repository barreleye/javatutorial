package shuo;

//abstract class
public abstract class Associate {
	
	final int age = 5;
	static String company = "Chase";
	
	//instance variables
	String name;
	
	//constructor
	public Associate(){}
	
	//static method
	public static String getCompanyName(){
		return company;
	}
	
	//abstract method
	public abstract void dowork();
	
	public final void daily(){
		System.out.println("Start work");
		dowork();
		System.out.println("Finished work");
	}

}
