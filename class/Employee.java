package shuo;


public class Employee {

	//fields
	public int id;
	private String name;
	protected double salary;
	//access modifier: private, public, protected, default
	protected static String company = "BOA";
	
	//constructors
	public Employee()
	{
		int i=0;
	}

	public Employee(int i, String name, double s)
	{
		id = i;
		//this refers to current object
		this.name = name;
		salary = s;
	}
	
	public Employee(int i, String n)
	{
		id = i;
		name = n;
	}
	
	//methods
	public String getName()
	{
		return name;
	}
	
	public double getSalary()
	{
		return salary;
	}
	
	protected void setNameSalary(String n, double s)
	{
		salary = s;
		name = n;
	}
	
	public String toString(){
		return "id:"+id+",name:"+name+",salary:"+getSalary()+",company:"+company;
	}
	
	public boolean equals(Object another)
	{
		Employee b = (Employee)another;
		if(this.name!=b.getName())
			return false;
		else if(this.id!=b.id)
			return false;
		else if(this.salary!=b.salary)
			return false;
		else if(this.company!=b.company)
			return false;
		else
			return true;
			
	}
}

