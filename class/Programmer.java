package shuo;

public class Programmer extends Associate implements ProgrammerRoutine,MeetingOrganizer{

	public void dowork()
	{
		doJava();
		doPython();
		organize();
	}
	
	public void doJava(){
		System.out.println("Programming in Java");
	}
	
	public void doPython(){
		System.out.println("Programming in Python");
	}
	
	public void organize(){
		System.out.println("Organize a meeting");
	}


}
