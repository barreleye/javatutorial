package shuo;

public class Shuo {

	public static void main(String[] args) {
		
		Shuo s = new Shuo();
		int c = s.add(2,3);
		double d = s.add(2, 3.0);
	}
	
	public int add(int a, int b)
	{
		return a+b;
	}
	
	public int add(int x, int y, int c)
	{
		return x+y+c;
	}
	
	public double add(int t, double s)
	{
		return t+s;
	}

}
