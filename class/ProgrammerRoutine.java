package shuo;

public interface ProgrammerRoutine {
	
	public static final int NUM_OF_PROGRAMMING_LANGUAGE = 2;
	
	public abstract void doJava();
	public abstract void doPython();

}
